from background_task import background
from functools import partial
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import math
import os
import pika
import time
import types
import zipfile

@csrf_exempt
def index(request):
    response = {}
    if 'X-ROUTING-KEY' in request.headers:
        file = request.FILES['file']
        fileName = request.FILES['file'].name
        with open(fileName, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        routing_key = request.headers.get('X-ROUTING-KEY')
        compressFileRequest(routing_key, fileName)
        response['success'] = "True"
        return JsonResponse(response)
    else:
        response['success'] = "False"
        return JsonResponse(response)

@background(schedule=1)
def compressFileRequest(routing_key, fileName):
    compressed_file = "file_compressed.bz2"
    progress.bytes = 0
    progress.obytes = 0
    with zipfile.ZipFile(compressed_file, 'w', compression=zipfile.ZIP_DEFLATED) as myzip:
        myzip.fp.write = types.MethodType(partial(progress, routing_key, os.path.getsize(fileName), myzip.fp.write), myzip.fp)
        myzip.write(fileName)
    myzip.close()
    os.remove(fileName)
    time.sleep(0.25)
    sendMessage(routing_key, "File has been compressed!")

def progress(routing_key, total_size, original_write, self, buf):
    progress.bytes += len(buf)
    progress.obytes += 1024 * 8
    print("{} bytes written".format(progress.bytes))
    print("{} original bytes handled".format(progress.obytes))
    percentage = int(100 * progress.obytes / total_size)
    print("{} % done".format(percentage))
    message = "File compression progress: " + str(roundDown(percentage)) + "%"
    sendMessage(routing_key, message)
    return original_write(buf)

def roundDown(x):
    result = int(math.floor(x / 10.0)) * 10
    if result > 100:
        return 100
    else:
        return result 

# from rabbitmq tutorial https://www.rabbitmq.com/tutorials/tutorial-three-python.html
def sendMessage(routing_key, message):
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    connectionParameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='1606833476', exchange_type='direct')
    channel.queue_declare(queue=routing_key)
    channel.basic_publish(exchange='1606833476', routing_key=routing_key, body=message)
    print(" [x] Sent %r:%r" % (routing_key, message))
    connection.close()
