from django.shortcuts import render
from .forms import FileForm

import json
import os
import uuid
import requests

def index(request):
    response = {}
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'base.html', response)

def upload(request):
    response = {}
    if request.POST:
        fileForm = FileForm(request.POST, request.FILES)
        if fileForm.is_valid:
            routingKey = unixID()
            response['routingKey'] = routingKey
            file = request.FILES['file']
            fileName = request.FILES['file'].name
            with open(fileName, 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            url = 'http://152.118.148.95:20336/index/'
            headers = {'X-ROUTING-KEY': routingKey}
            files = {'file': open(fileName,'rb')}
            r = requests.post(url, headers=headers, files=files)
            os.remove(fileName)
            response['success'] = r.json().get('success')
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'base.html', response) 

def unixID():
    stringLength = 8
    randomString = uuid.uuid4().hex
    randomString  = randomString.upper()[0:stringLength]
    return randomString
